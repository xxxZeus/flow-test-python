from . import node_abstract

class OutboundNode(node_abstract.Node):
  def __init__(self, data, config, nodeId):
    super().__init__(data, config, nodeId)
    # now we do additional checks for this node
    # we need delay attr for this node
    # include checks for template id and channel here, not doing it
    

  def perform(self):
    print("Outbound execution of node", self.id)
		# write email and sms stuff here
		
  def resolveNextNodes(self):
    # next nodes if any, should have delay of 0
    return list(map(lambda x: (x, 0), self.config["nextNodes"]))
        
        
