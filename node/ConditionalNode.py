from . import node_abstract

from operation import EqualToOperation
from operation import LessThanOperation


# for easy resolving so that we dont have to use if else 
operationMap = {
  "EqualTo": EqualToOperation.EqualToOperation,
  "LessThan": LessThanOperation.LessThanOperation
}

class ConditionalNode(node_abstract.Node):
  def __init__(self, data, config, nodeId):
    super().__init__(data, config, nodeId)
    # now we do additional checks for this node
    if "condition" not in config:
      raise Exception("Condition attr missing", nodeId)
    if "operation" not in config["condition"] or config["condition"]["operation"] not in operationMap:
      raise Exception("Invalid Operation", nodeId)
    # execpted condition: {"attribute":xyz, "operation": xyz, "targetValue": xyz}
    # more checks needed here to see if conditons are correct  
    # ideally we will have array of conditions with and or conditions, but this just eg so not implementing that   

    # we will have only two next nodes in condional nodes, if result of oper = true then we go for first node to be resolved as next
    # else we go for second
    if len(config["nextNodes"]) != 2:
      raise Exception("Only Two next nodes allowed for Conditional Nodes")
    

  def perform(self):     
    val1 = self.data[ self.config["condition"]["attribute"] ]  # pick up value to compare from data
    val2 = self.config["condition"]["targetValue"] # value to compare against  
    oper = operationMap[ self.config["condition"]["operation"] ](val1, val2)
    self.result = oper.valid() # store result to be used in resolve next nodes    

    

  def resolveNextNodes(self):
    # if self.result = true we return first node else the second
    if self.result == True:
      return [(self.config["nextNodes"][0], 0),] # first node with 0 based indexing and 0 timedelay
    return [(self.config["nextNodes"][1], 0),] # else second node with 0 based indexing and 0 timedelay
    
    
      
