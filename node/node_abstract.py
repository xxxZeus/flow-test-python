from abc import ABC, abstractmethod

class Node(ABC):
  def __init__(self, data, config, nodeId):
    self.data = data 
    if "nextNodes" not in config:
      raise Exception("nextNodes attr missing from config", nodeId)
    self.config = config
    self.id = nodeId
    # config has following mandatory attributes:
    # conig.nextNodes = [string, ..] - list of next node-ids, 
    '''
    in child classes if you override constructor call super().__init__(data, config) 
    to make sure checks here passes first
    '''
      
  @abstractmethod
  def perform():
    pass 
    # performs the task
      
  @abstractmethod
  def resolveNextNodes():
    pass
    # returns list of next nodes' id to be executed along with time delay
    # eg [ ('node-1', 0), ('node-2', 5) ]
    # execute node-1 immediately, execute node-2 after 5 units of time, via celery delayed jobs 
    # eg to return nodes with delay 0: return return list(map(lambda x: (x, 0), self.config["nextNodes"]))

    
    