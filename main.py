import json 
import time 
# nodes
from node.TimeDelayNode import TimeDelayNode
from node.ConditionalNode import ConditionalNode 
from node.OutboundNode import OutboundNode 
from node.TriggerNode import TriggerNode

# to avoid if and else conditions later on
# can define constant class to replace strings here with integers, eg Constants.TimeDelay, for simplicity sticking with strings here
nodeMap = {
  "TimeDelay": TimeDelayNode,
  "Conditional": ConditionalNode,
  "Outbound": OutboundNode,
  "Trigger": TriggerNode
}


# run time code, this will be a celery function:
# @app.task 
def execFlow(flow, nodesToBeExecuted):
  # flow: json string of flow, see below for sample flow json 
  # flow: ideally it should be flowId which would be loaded here from DB, for simplicity keeping full flow string here
  # nodesToBeExecuted: id of nodes to be executed next
  # at first call nodesToBeExected = [ "triggerNodeId" ]    
  flowJson = json.loads(flow)
  nextNodes = [] # list of next nodes after executing current Nodes
  for nodeId in nodesToBeExecuted:
    node = flowJson["nodes"][nodeId] # node info
    # now we will instantiate node class object according to type
    nodeObj = nodeMap[ node["type"] ](flowJson["data"], node["config"], nodeId)
    nodeObj.perform() # perform the task the node is meant to do
    next = nodeObj.resolveNextNodes() # we get array of tuples, of next node ids and delays, eg: [ ("nodeId2", 0), ("nodeId3", 5)] # exec nodeId2 immediately and nodeId3 with delay of 5 units
    nextNodes.extend(next) # add to next nodes list   
  for nextNode, delay in nextNodes:
    print(":: queing execution of node", nextNode, "with delay", delay)
    # use celery apply_async to delay task here but we will just simulate that using sleep here
    time.sleep(delay)
    execFlow(flow, [nextNode,]) # we can club nodes with same delay and pass it as an array here, for eg im not doing it rn 
    # in celery we would have had some thing like:
    # execFlow.apply_async(args=[flow, [nextNode,]], countdown=delay)

flow1 = ""
with open("flow1.json", "r") as f:
  flow1 = f.read()

print(":: Starting flow 1 exec")  
execFlow(flow1, ["nodeId1",])


flow2 = ""
with open("flow2.json", "r") as f:
  flow2 = f.read()

print(":: Starting flow 2 exec")  
execFlow(flow2, ["nodeId1",])
      







