from . import operation_abstract

class LessThanOperation(operation_abstract.Operation):
  def valid(self):
    return self.val1 < self.val2
