from abc import ABC, abstractmethod

class Operation(ABC):
  def __init__(self, val1, val2):
    self.val1 = val1
    self.val2 = val2

  @abstractmethod
  def valid(self):
    # return type: boolean, performs operation and returns result
    pass 